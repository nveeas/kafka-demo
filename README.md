# kafka-demo

   groupId: io.bh.bhe.nve.dsp
artifactId: kafka-demo
   version: DEV-SNAPSHOT


 The kafka producer for the kafka-demo
 
 ## Building
 From the project's home directory, run the following command:
 ```
  $ ./mvnw clean package
 ```
 
 ## Running
 From the project's home directory, run the following command:
```
 $ ./bin/runLocal.sh
```
 This script will bootstrap the any necessary runtime properties and start the local instance.
 
 
 ## CI/CD
 The following provides information on setting up a 'Continuous Integration / Continuous Deployment' pipeline.  
 
 ### Defining the kafka-demo Concourse Pipeline
 The following pipelines have been provided by default, these are:
 * ci/pipeline-dev-and-dr-retry.yml -- Pipeline for 'DEV' and 'DR' with retires ...
 * ci/pipeline-tst-and-dr-retry.yml -- Pipeline for 'TST' and 'DR' with retries ...
 * ci/pipeline-stg.yml              -- Pipeline for 'STG' ...
 
 
_To obtain the latest defined pipelines, if not already cloned, Clone the Generic Pipelines' git Repository:_
 _**(Directory structure may vary depending upon where you stow source code...)**_
```
    $ cd ~/code
    $ git clone git@bitbucket.org:nveeas/generic-pipelines.git
```
Now compare or replace as necessary the default pipelines with an updated version.  You may need to check the variables, as
you may need to adjust for the new Pipeline version, but may not be necessary depending upon the pipeline and associated changes.

### Fly the Pipeline on Concourse
 * Now perform a **fly** command to create (or modify) the initial **DEV** pipeline on Concourse:
   Navigate to this project's home directory and enter the following command:
```
$ fly -t dev-apps set-pipeline -p kafka-demo-dev -l ci/vars-dev-and-dr-retry.yml \
 	-c ci/pipeline-dev-and-dr-retry.yml
 	
```
 You should see a message echoing back the validated Pipeline configuration yaml.
 You will be promoted to accept or not to publish the new pipeline.
 Once published or created, the pipeline will be in a **_paused_** state.
 
 To unpause the pipeline, proceed to use the following command or via the Concourse UI:
 ```
$ fly -t dev-apps unpause-pipeline -p kafka-demo-dev
``` 

 If you wish to ever destroy the pipeline perform the following command:
```
fly -t dev-apps destroy-pipeline --pipeline kafka-demo-dev
     
```
  The pipeline once destroyed, can easily be recreated by using the previous'set-pipeline'
  command above.
   	
### Running the Pipeline
To run the pipeline for the first time, go to the Concourse UI and 'un-pause' the pipeline and 
now initiate the first build by hitting the 'plus sign' on the upper right of the display in concourse.

By default, the 'master' branch, unless modified to a different branch, will automatically trigger a pipeline to run after a commit/merge
occurs within BitBucket. 

### Getting Tanzu Logs
To obtain the most recent logs generated from the application/service, perform
the following **cf** command:
```
$ cf logs kafka-demo-dev --recent
```

To obtain a tailing of the logs simple issue the following command:
```
$ cf logs kafka-demo-dev 
```
This will tail the service logs until you end the tailing with a normal, <CTRL>-C to end the command.

Note: You can easily pipe the output of the logs to any other process or necessary filter such as grep to look for certain messages if needed.

### Access 
To access the kafka-demo DEV Instance:
Go to the following URL: https://kafka-demo-dev.apps.dev.cm.spr.bz

Actuator URL EndPoint: http://kafka-demo-dev.apps.dev.cm.spr.bz/dsp


## Tanzu Platform Tools
 The following additional tools you will need to manage and deploy your
 Application/Service.  
 
 ### cf
 * Download the applicable **cf** cli tool from Cloud Foundry:
   * WIN: https://packages.cloudfoundry.org/stable?release=windows64-exe&source=github
   * Other: https://github.com/cloudfoundry/cli
   
   
 ### fly
 * Download the **fly** cli command from our Internal Concourse Server
    * WIN: https://concourse-nonprod.cm.spr.bz/api/v1/cli?arch=amd64&platform=windows
    * LINUX: https://concourse-nonprod.cm.spr.bz/api/v1/cli?arch=amd64&platform=linux
    * MAC: https://concourse-nonprod.cm.spr.bz/api/v1/cli?arch=amd64&platform=darwin
    
    Note: Always use the correct CLI version per our current Concourse instance and upgrade when the platform team
     deems necessary.
