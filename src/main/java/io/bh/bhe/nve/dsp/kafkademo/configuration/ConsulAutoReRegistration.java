package io.bh.bhe.nve.dsp.kafkademo.configuration;

import com.ecwid.consul.v1.ConsulClient;
import com.ecwid.consul.v1.Response;
import com.ecwid.consul.v1.catalog.CatalogServiceRequest;
import com.ecwid.consul.v1.catalog.model.CatalogService;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.client.ConditionalOnDiscoveryEnabled;
import org.springframework.cloud.client.serviceregistry.AbstractAutoServiceRegistration;
import org.springframework.cloud.consul.ConditionalOnConsulEnabled;
import org.springframework.cloud.consul.serviceregistry.ConsulRegistration;
import org.springframework.context.SmartLifecycle;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.atomic.AtomicBoolean;

@Log4j2
@Configuration
@ConditionalOnProperty(value = "spring.cloud.consul.enabled", matchIfMissing = true)
@ConditionalOnConsulEnabled
@ConditionalOnDiscoveryEnabled
public class ConsulAutoReRegistration implements SmartLifecycle {
    private final ConsulClient client;
    private final AbstractAutoServiceRegistration<ConsulRegistration> serviceRegistration;
    private final ConsulRegistration consulRegistration;
    private final TaskScheduler taskScheduler = getTaskScheduler();
    private final AtomicBoolean running = new AtomicBoolean(false);
    private ScheduledFuture<?> watchFuture;

    public ConsulAutoReRegistration(ConsulClient client,
                                    AbstractAutoServiceRegistration<ConsulRegistration> serviceRegistration,
                                    ConsulRegistration consulRegistration) {
        super();
        this.client = client;
        this.serviceRegistration = serviceRegistration;
        this.consulRegistration = consulRegistration;
    }

    @Bean
    @ConditionalOnMissingBean
    public ConsulAutoReRegistration createConsulAutoReRegistration(ConsulClient consulClient,
            AbstractAutoServiceRegistration<ConsulRegistration> serviceRegistration,
            ConsulRegistration consulRegistration) {
        return new ConsulAutoReRegistration(consulClient, serviceRegistration, consulRegistration);
    }

    private static ThreadPoolTaskScheduler getTaskScheduler() {
        ThreadPoolTaskScheduler taskScheduler = new ThreadPoolTaskScheduler();
        taskScheduler.initialize();
        return taskScheduler;
    }

    @Override
    public boolean isAutoStartup() {
        return true;
    }

    @Override
    public void stop(Runnable callback) {
        this.stop();
        callback.run();
    }

    @Override
    public void start() {
        if (this.running.compareAndSet(false, true)) {

            this.watchFuture = this.taskScheduler.scheduleWithFixedDelay(this::doCheck, 13000);
        }
    }

    @Override
    public void stop() {
        if (this.running.compareAndSet(true, false) && this.watchFuture != null) {
            this.watchFuture.cancel(true);
        }
    }

    @Override
    public boolean isRunning() {
        return false;
    }

    @Override
    public int getPhase() {
        return 0;
    }

    public void doCheck() {
        String applicationName = consulRegistration.getServiceId();
        String registeredName = consulRegistration.getInstanceId();
        String myHostname = consulRegistration.getHost();

        if (!serviceRegistration.isRunning()) {
            // nothing to do
            return;
        }

        try {
            Response<List<CatalogService>> services = this.client.getCatalogService(applicationName,
                    CatalogServiceRequest.newBuilder().build());
            Optional<CatalogService> catalogService = services.getValue().stream()
                    .filter(service -> service.getServiceId().equals(registeredName)
                            && service.getServiceAddress().equals(myHostname))
                    .findAny();

            if (!catalogService.isPresent()) {
                // reregister my self
                log.warn("Did not find {} in the Consul registry, do re-register", applicationName);

                serviceRegistration.stop();
                serviceRegistration.start();
            }
        } catch (Exception e) {
            log.error("Couldn't query Consul catalog service for {}", applicationName, e);
        }
    }
}
