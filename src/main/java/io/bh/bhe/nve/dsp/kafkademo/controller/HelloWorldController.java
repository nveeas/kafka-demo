package io.bh.bhe.nve.dsp.kafkademo.controller;

import lombok.extern.log4j.Log4j2;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;
import java.util.Date;

@Log4j2
@RestController
public class HelloWorldController {


    @GetMapping(name = "/", produces = { "application/json", "application/xml" })
    @ResponseBody
    public ResponseEntity<Object> get() {
        return new ResponseEntity<>("Hello from " + HelloWorldController.class.getSimpleName() + " at "
                + Date.from(Instant.now()) + ".", HttpStatus.OK);
    }

    @GetMapping(name = "/", produces = { "text/plain" })
    @ResponseBody
    public ResponseEntity<Object> getPlainText() {
        return new ResponseEntity<>("Hello from " + HelloWorldController.class.getSimpleName() + " at "
                + Date.from(Instant.now()) + ".", HttpStatus.OK);
    }

    @GetMapping(name = "/", produces = { "text/html" })
    @ResponseBody
    public ResponseEntity<Object> getHTML() {
        return new ResponseEntity<>("<html><head><title>Hello</title></head><body><h2>Hello from "
                + HelloWorldController.class.getSimpleName() + " at " + Date.from(Instant.now()) + "."
                + "</h2></body></html>", HttpStatus.OK);
    }

}
